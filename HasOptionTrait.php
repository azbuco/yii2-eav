<?php

namespace azbuco\eav;

use yii\base\InvalidArgumentException;
use yii\base\InvalidParamException;

/**
 * Trait for ActiveRecord to handle related EAV option models
 */
trait HasOptionTrait
{

    public function getOption($key, $default = null)
    {
        $class = $this->optionModelClass();
        
        $option = $class::find()
        ->where([$this->optionModelLinkAttribute() => $this->id])
        ->andWhere(['key' => $key])
        ->one();

        return $option ? $option->getValue() : $default;
    }

    /**
     * Set a EAV record
     * @param string $key
     * @param mixed $value when a values is set to null, the corresponding record will be removed from the database
     * @param string $type the value type, if null automatically detected. See SectionOptions TYPE constants
     * @return EavInterface|null
     * @throws InvalidArgumentException
     */
    public function setOption($key, $value, $type = null)
    {
        $class = $this->optionModelClass();
        $linkAttribute = $this->optionModelLinkAttribute();
        
        $model = $class::findOne([$linkAttribute => $this->id, 'key' => $key]);

        if ($value === null) {
            if ($model !== null) {
                $model->delete();
            }
            return null;
        }

        if ($model === null) {
            $model = new $class();
            $model->$linkAttribute = $this->id;
            $model->key = $key;
        }
        
        $model->int_value = null;
        $model->num_value = null;
        $model->string_value = null;
        $model->text_value = null;
        $model->json_value = null;

        $type = $type ?? $class::quessType($value);
        
        switch ($type) {
            case EavInterface::TYPE_INT:
                $model->int_value = intval($value);
                break;
            case EavInterface::TYPE_NUM:
                $model->num_value = floatval($value);
                break;
            case EavInterface::TYPE_STRING:
                $model->string_value = $value;
                break;
            case EavInterface::TYPE_TEXT:
                $model->text_value = $value;
                break;
            case EavInterface::TYPE_JSON:
                $model->json_value = $value;
                break;
            default:
                throw new InvalidArgumentException('Unknown type "' . $type . '"');
        }

        if (!$model->save()) {
            throw new InvalidArgumentException(array_values($model->getFirstErrors())[0]);
        }

        return $model;
    }

}
