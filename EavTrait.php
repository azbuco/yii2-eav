<?php

namespace azbuco\eav;

use yii\base\InvalidParamException;
use yii\helpers\Json;

/**
 * Trait for ActiveRecord to handle EAV (entity-attribute-value) models
 */
trait EavTrait
{
    public $_json_value;
    
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }
        
        if (empty($this->json_value) || $this->json_value === '""') {
            $this->json_value = null;
        } else if (is_array($this->json_value)) {
            $this->json_value = Json::encode($this->json_value);
        }

        return true;
    }

    public function afterFind()
    {
        $this->_json_value = $this->json_value;
        $this->json_value = Json::decode($this->json_value);

        parent::afterFind();
    }

    public function setValue($value, $type = null) {
        if ($type === null) {
            $type = $this->quessType($value);
        }
        
        switch ($type) {
            case EavInterface::TYPE_INT:
                $this->int_value = intval($value);
                break;
            case EavInterface::TYPE_NUM:
                $this->num_value = floatval($value);
                break;
            case EavInterface::TYPE_STRING:
                $this->string_value = $value;
                break;
            case EavInterface::TYPE_TEXT:
                $this->text_value = $value;
                break;
            case EavInterface::TYPE_JSON:
                $this->json_value = $value;
                break;
            default:
                throw new InvalidParamException('Unknown type "' . $type . '"');
        }
    }
    
    public function getValue()
    {
        return $this->int_value ?? $this->num_value ?? $this->string_value ?? $this->text_value ?? $this->json_value;
    }
    
    /**
     * Quesses the type of a value
     * @param mixed $value
     * @return string
     * @throws InvalidParamException
     */
    public static function quessType($value)
    {
        if (is_int($value)) {
            return EavInterface::TYPE_INT;
        }
        
        if (is_float($value)) {
            return EavInterface::TYPE_NUM;
        }

        if (is_string($value) && strlen($value) < 255) {
            return EavInterface::TYPE_STRING;
        }
        
        if (is_string($value)) {
            return EavInterface::TYPE_TEXT;
        }

        if (is_array($value)) {
            return EavInterface::TYPE_JSON;
        }

        throw new InvalidParamException('Unable to detect the type of the passed value.');
    }

}
