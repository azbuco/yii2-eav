<?php

namespace azbuco\eav;

/**
 * Interface for ActiveRecord to handle EAV (entity-attribute-value) models
 */
interface EavInterface {

    /**
     * EAV types
     */
    const TYPE_INT = 'int';
    const TYPE_NUM = 'num';
    const TYPE_STRING = 'string';
    const TYPE_TEXT = 'text';
    const TYPE_JSON = 'json';

    public function setValue($value, $type = null);

    public function getValue();
}
