# Yii2 EAV #

Allow ActiveRecord to handle EAV model

## Usage

Create an options table like the following:

```
$this->createTable(
'{{%[PARENT]_options}}', [
    'id' => $this->primaryKey(),
    '[PARENT]_id' => $this->integer(),
    'key' => $this->string(),
    'int_value' => $this->integer(),
    'num_value' => $this->decimal(18, 6),
    'string_value' => $this->string(255),
    'text_value' => $this->text(),
    'json_value' => $this->text(),
    'created_at' => $this->integer(),
    'created_by' => $this->integer(),
    'updated_at' => $this->integer(),
    'updated_by' => $this->integer(),
], $tableOptions);

$this->createIndex('idx-[PARENT]_options-[PARENT]_id', '{{%[PARENT]_options}}', ['[PARENT]_id', 'key'], true);
$this->addForeignKey('fk-[PARENT]_options-[PARENT]_id', '{{%[PARENT]_options}}', ['[PARENT]_id'], '{{%[PARENT]}}', 'id', 'CASCADE', 'CASCADE');
```

Generate the models with Gii.

Apply the following modifications:

1. Implement into the option model:
```
class [PARENT]Options implements \azbuco\eav\EavInterface
{
    use \azbuco\eav\EavTrait;
    // ...
}
```

2. Implement into the parent model:
```
class [PARENT] implements HasOptionInterface
{
    use HasOptionTrait;

    // ...

    public function optionModelClass()
    {
        return [PARENT]Options::class;
    }

    public function optionModelLinkAttribute()
    {
        return '[PARENT]_id';
    }

    // ...
}
```
