<?php

namespace azbuco\eav;

interface HasOptionInterface
{

    public function optionModelClass();
    
    public function optionModelLinkAttribute();

    public function getOption($key, $default = null);

    public function setOption($key, $value, $type = null);
}
